package com.olide;

import com.intellij.ide.structureView.*;
import com.intellij.ide.util.treeView.smartTree.Sorter;
import com.intellij.psi.PsiFile;
import com.olide.psi.OlangFile;
import org.jetbrains.annotations.NotNull;

public class OlangStructureViewModel extends StructureViewModelBase implements
    StructureViewModel.ElementInfoProvider {
  public OlangStructureViewModel(PsiFile psiFile) {
    super(psiFile, new OlangStructureViewElement(psiFile));
  }

  @NotNull
  public Sorter[] getSorters() {
    return new Sorter[]{Sorter.ALPHA_SORTER};
  }


  @Override
  public boolean isAlwaysShowsPlus(StructureViewTreeElement element) {
    return false;
  }

  @Override
  public boolean isAlwaysLeaf(StructureViewTreeElement element) {
    return element instanceof OlangFile;
  }
}