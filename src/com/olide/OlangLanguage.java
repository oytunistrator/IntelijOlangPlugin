package com.olide;

import com.intellij.lang.Language;

public class OlangLanguage extends Language {
  public static final OlangLanguage INSTANCE = new OlangLanguage();

  private OlangLanguage() {
    super("Olang");
  }
}