package com.olide;

import com.intellij.psi.codeStyle.*;

public class OlangCodeStyleSettings extends CustomCodeStyleSettings {
  public OlangCodeStyleSettings(CodeStyleSettings settings) {
    super("OlangCodeStyleSettings", settings);
  }
}