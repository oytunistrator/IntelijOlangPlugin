package com.olide;

import com.intellij.codeInsight.daemon.*;
import com.intellij.codeInsight.navigation.NavigationGutterIconBuilder;
import com.intellij.openapi.project.Project;
import com.intellij.psi.*;
import com.olide.psi.OlangProperty;
import org.jetbrains.annotations.NotNull;

import java.util.*;

public class OlangLineMarkerProvider extends RelatedItemLineMarkerProvider {
  @Override
  protected void collectNavigationMarkers(@NotNull PsiElement element,
                                          Collection<? super RelatedItemLineMarkerInfo> result) {
    if (element instanceof PsiLiteralExpression) {
      PsiLiteralExpression literalExpression = (PsiLiteralExpression) element;
      String value = literalExpression.getValue() instanceof String ? (String) literalExpression.getValue() : null;
      if (value != null && value.startsWith("olang" + ":")) {
        Project project = element.getProject();
        final List<OlangProperty> properties = OlangUtil.findProperties(project, value.substring(7));
        if (properties.size() > 0) {
          NavigationGutterIconBuilder<PsiElement> builder =
              NavigationGutterIconBuilder.create(OlangIcons.FILE).
                  setTargets(properties).
                  setTooltipText("Navigate to a simple property");
          result.add(builder.createLineMarkerInfo(element));
        }
      }
    }
  }
}
