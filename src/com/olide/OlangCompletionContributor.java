package com.olide;

import com.intellij.codeInsight.completion.*;
import com.intellij.codeInsight.lookup.LookupElementBuilder;
import com.intellij.patterns.PlatformPatterns;
import com.intellij.util.ProcessingContext;
import com.olide.psi.OlangTypes;
import org.jetbrains.annotations.NotNull;

public class OlangCompletionContributor extends CompletionContributor {
  public OlangCompletionContributor() {
    extend(CompletionType.BASIC,
           PlatformPatterns.psiElement(OlangTypes.VALUE).withLanguage(OlangLanguage.INSTANCE),
           new CompletionProvider<CompletionParameters>() {
             public void addCompletions(@NotNull CompletionParameters parameters,
                                        ProcessingContext context,
                                        @NotNull CompletionResultSet resultSet) {
               resultSet.addElement(LookupElementBuilder.create("Function"));
             }
           }
    );
  }
}
