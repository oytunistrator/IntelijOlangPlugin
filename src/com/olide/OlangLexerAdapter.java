package com.olide;

import com.intellij.lexer.FlexAdapter;

import java.io.Reader;

public class OlangLexerAdapter extends FlexAdapter {
  public OlangLexerAdapter() {
    super(new OlangLexer((Reader) null));
  }
}
