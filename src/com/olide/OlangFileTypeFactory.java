package com.olide;

import com.intellij.openapi.fileTypes.*;
import org.jetbrains.annotations.NotNull;

public class OlangFileTypeFactory extends FileTypeFactory {
  @Override
  public void createFileTypes(@NotNull FileTypeConsumer fileTypeConsumer) {
    fileTypeConsumer.consume(OlangFileType.INSTANCE, "of");
    fileTypeConsumer.consume(OlangFileType.INSTANCE, "om");
    fileTypeConsumer.consume(OlangFileType.INSTANCE, "ol");
  }
}