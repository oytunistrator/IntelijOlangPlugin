package com.olide.psi;

import com.intellij.openapi.project.Project;
import com.intellij.psi.*;
import com.olide.OlangFileType;

public class OlangElementFactory {
  public static OlangProperty createProperty(Project project, String name, String value) {
    final OlangFile file = createFile(project, name + " = " + value);
    return (OlangProperty) file.getFirstChild();
  }

  public static OlangProperty createProperty(Project project, String name) {
    final OlangFile file = createFile(project, name);
    return (OlangProperty) file.getFirstChild();
  }

  public static PsiElement createCRLF(Project project) {
    final OlangFile file = createFile(project, "\n");
    return file.getFirstChild();
  }

  public static OlangFile createFile(Project project, String text) {
    String name = "dummy.olang";
    return (OlangFile) PsiFileFactory.getInstance(project).
        createFileFromText(name, OlangFileType.INSTANCE, text);
  }
}