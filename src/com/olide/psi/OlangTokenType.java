package com.olide.psi;

import com.intellij.psi.tree.IElementType;
import com.olide.OlangLanguage;
import org.jetbrains.annotations.*;

public class OlangTokenType extends IElementType {
  public OlangTokenType(@NotNull @NonNls String debugName) {
    super(debugName, OlangLanguage.INSTANCE);
  }

  @Override
  public String toString() {
    return "OlangTokenType." + super.toString();
  }
}