package com.olide.psi;

import com.intellij.psi.tree.IElementType;
import com.olide.OlangLanguage;
import org.jetbrains.annotations.*;

public class OlangElementType extends IElementType {
  public OlangElementType(@NotNull @NonNls String debugName) {
    super(debugName, OlangLanguage.INSTANCE);
  }
}