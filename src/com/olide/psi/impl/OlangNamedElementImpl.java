package com.olide.psi.impl;

import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.intellij.lang.ASTNode;
import com.olide.psi.OlangNamedElement;
import org.jetbrains.annotations.NotNull;

public abstract class OlangNamedElementImpl extends ASTWrapperPsiElement implements OlangNamedElement {
  public OlangNamedElementImpl(@NotNull ASTNode node) {
    super(node);
  }
}