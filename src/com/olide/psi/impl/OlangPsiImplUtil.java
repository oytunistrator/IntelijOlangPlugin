package com.olide.psi.impl;

import com.intellij.lang.ASTNode;
import com.intellij.navigation.ItemPresentation;
import com.intellij.psi.*;
import com.olide.OlangIcons;
import com.olide.psi.*;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

public class OlangPsiImplUtil {
  public static String getKey(OlangProperty element) {
    ASTNode keyNode = element.getNode().findChildByType(OlangTypes.KEY);
    if (keyNode != null) {
      // IMPORTANT: Convert embedded escaped spaces to Olang spaces
      return keyNode.getText().replaceAll("\\\\ ", " ");
    } else {
      return null;
    }
  }

  public static String getValue(OlangProperty element) {
    ASTNode valueNode = element.getNode().findChildByType(OlangTypes.VALUE);
    if (valueNode != null) {
      return valueNode.getText();
    } else {
      return null;
    }
  }

  public static String getName(OlangProperty element) {
    return getKey(element);
  }

  public static PsiElement setName(OlangProperty element, String newName) {
    ASTNode keyNode = element.getNode().findChildByType(OlangTypes.KEY);
    if (keyNode != null) {
      OlangProperty property = OlangElementFactory.createProperty(element.getProject(), newName);
      ASTNode newKeyNode = property.getFirstChild().getNode();
      element.getNode().replaceChild(keyNode, newKeyNode);
    }
    return element;
  }

  public static PsiElement getNameIdentifier(OlangProperty element) {
    ASTNode keyNode = element.getNode().findChildByType(OlangTypes.KEY);
    if (keyNode != null) {
      return keyNode.getPsi();
    } else {
      return null;
    }
  }

  public static ItemPresentation getPresentation(final OlangProperty element) {
    return new ItemPresentation() {
      @Nullable
      @Override
      public String getPresentableText() {
        return element.getKey();
      }

      @Nullable
      @Override
      public String getLocationString() {
        PsiFile containingFile = element.getContainingFile();
        return containingFile == null ? null : containingFile.getName();
      }

      @Nullable
      @Override
      public Icon getIcon(boolean unused) {
        return OlangIcons.FILE;
      }
    };
  }
}
