package com.olide.psi;

import com.intellij.extapi.psi.PsiFileBase;
import com.intellij.openapi.fileTypes.FileType;
import com.intellij.psi.FileViewProvider;
import com.olide.*;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;

public class OlangFile extends PsiFileBase {
  public OlangFile(@NotNull FileViewProvider viewProvider) {
    super(viewProvider, OlangLanguage.INSTANCE);
  }

  @NotNull
  @Override
  public FileType getFileType() {
    return OlangFileType.INSTANCE;
  }

  @Override
  public String toString() {
    return "Olang File";
  }

  @Override
  public Icon getIcon(int flags) {
    return super.getIcon(flags);
  }
}