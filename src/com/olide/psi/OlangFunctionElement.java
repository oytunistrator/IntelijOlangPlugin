package com.olide.psi;

import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiNameIdentifierOwner;


public interface OlangFunctionElement extends PsiNameIdentifierOwner {
}