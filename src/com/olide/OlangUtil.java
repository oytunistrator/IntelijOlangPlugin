package com.olide;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiManager;
import com.intellij.psi.search.*;
import com.intellij.psi.util.PsiTreeUtil;
import com.intellij.util.indexing.FileBasedIndex;
import com.olide.psi.*;

import java.util.*;

public class OlangUtil {
  public static List<OlangProperty> findProperties(Project project, String key) {
    List<OlangProperty> result = null;
    Collection<VirtualFile> virtualFiles =
        FileBasedIndex.getInstance().getContainingFiles(FileTypeIndex.NAME, OlangFileType.INSTANCE,
                                                        GlobalSearchScope.allScope(project));
    for (VirtualFile virtualFile : virtualFiles) {
      OlangFile olangFile = (OlangFile) PsiManager.getInstance(project).findFile(virtualFile);
      if (olangFile != null) {
        OlangProperty[] properties = PsiTreeUtil.getChildrenOfType(olangFile, OlangProperty.class);
        if (properties != null) {
          for (OlangProperty property : properties) {
            if (key.equals(property.getKey())) {
              if (result == null) {
                result = new ArrayList<OlangProperty>();
              }
              result.add(property);
            }
          }
        }
      }
    }
    return result != null ? result : Collections.<OlangProperty>emptyList();
  }

  public static List<OlangProperty> findProperties(Project project) {
    List<OlangProperty> result = new ArrayList<OlangProperty>();
    Collection<VirtualFile> virtualFiles =
        FileBasedIndex.getInstance().getContainingFiles(FileTypeIndex.NAME, OlangFileType.INSTANCE,
                                                        GlobalSearchScope.allScope(project));
    for (VirtualFile virtualFile : virtualFiles) {
      OlangFile olangFile = (OlangFile) PsiManager.getInstance(project).findFile(virtualFile);
      if (olangFile != null) {
        OlangProperty[] properties = PsiTreeUtil.getChildrenOfType(olangFile, OlangProperty.class);
        if (properties != null) {
          Collections.addAll(result, properties);
        }
      }
    }
    return result;
  }
}