package com.olide;

import com.intellij.formatting.*;
import com.intellij.lang.ASTNode;
import com.intellij.openapi.util.TextRange;
import com.intellij.psi.*;
import com.intellij.psi.codeStyle.CodeStyleSettings;
import com.olide.psi.OlangTypes;
import org.jetbrains.annotations.*;

public class OlangFormattingModelBuilder implements FormattingModelBuilder {
  @NotNull
  @Override
  public FormattingModel createModel(PsiElement element, CodeStyleSettings settings) {
    return FormattingModelProvider
        .createFormattingModelForPsiFile(element.getContainingFile(),
                new OlangBlock(element.getNode(),
                    Wrap.createWrap(WrapType.NONE, false),
                    Alignment.createAlignment(),
                    createSpaceBuilder(settings)),
                settings);
  }

  private static SpacingBuilder createSpaceBuilder(CodeStyleSettings settings) {
    return new SpacingBuilder(settings, OlangLanguage.INSTANCE)
        .around(OlangTypes.SEPARATOR)
        .spaceIf(settings.SPACE_AROUND_ASSIGNMENT_OPERATORS)
        .before(OlangTypes.PROPERTY)
        .none();
  }

  @Nullable
  @Override
  public TextRange getRangeAffectingIndent(PsiFile file, int offset, ASTNode elementAtOffset) {
    return null;
  }
}
