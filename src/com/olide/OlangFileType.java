package com.olide;

import com.intellij.openapi.fileTypes.LanguageFileType;
import org.jetbrains.annotations.*;

import javax.swing.*;

public class OlangFileType extends LanguageFileType {
  public static final OlangFileType INSTANCE = new OlangFileType();

  private OlangFileType() {
    super(OlangLanguage.INSTANCE);
  }

  @NotNull
  @Override
  public String getName() {
    return "Olang file";
  }

  @NotNull
  @Override
  public String getDescription() {
    return "Olang language file";
  }

  @NotNull
  @Override
  public String getDefaultExtension() {
    return "of";
  }

  @Nullable
  @Override
  public Icon getIcon() {
    return OlangIcons.FILE;
  }
}