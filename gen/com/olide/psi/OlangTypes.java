// This is a generated file. Not intended for manual editing.
package com.olide.psi;

import com.intellij.psi.tree.IElementType;
import com.intellij.psi.PsiElement;
import com.intellij.lang.ASTNode;
import com.olide.psi.impl.*;

public interface OlangTypes {

  IElementType FN = new OlangElementType("FN");
  IElementType PROPERTY = new OlangElementType("PROPERTY");

  IElementType COMMENT = new OlangTokenType("COMMENT");
  IElementType KEY = new OlangTokenType("KEY");
  IElementType SEPARATOR = new OlangTokenType("SEPARATOR");
  IElementType VALUE = new OlangTokenType("VALUE");

  class Factory {
    public static PsiElement createElement(ASTNode node) {
      IElementType type = node.getElementType();
       if (type == FN) {
        return new OlangFnImpl(node);
      }
      else if (type == PROPERTY) {
        return new OlangPropertyImpl(node);
      }
      throw new AssertionError("Unknown element type: " + type);
    }
  }
}
