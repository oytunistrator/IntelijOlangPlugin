// This is a generated file. Not intended for manual editing.
package com.olide.psi;

import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.PsiElement;

public class OlangVisitor extends PsiElementVisitor {

  public void visitFn(@NotNull OlangFn o) {
    visitFunctionElement(o);
  }

  public void visitProperty(@NotNull OlangProperty o) {
    visitNamedElement(o);
  }

  public void visitFunctionElement(@NotNull OlangFunctionElement o) {
    visitPsiElement(o);
  }

  public void visitNamedElement(@NotNull OlangNamedElement o) {
    visitPsiElement(o);
  }

  public void visitPsiElement(@NotNull PsiElement o) {
    visitElement(o);
  }

}
