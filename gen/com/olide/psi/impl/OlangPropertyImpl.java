// This is a generated file. Not intended for manual editing.
package com.olide.psi.impl;

import java.util.List;

import com.intellij.psi.PsiType;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static com.olide.psi.OlangTypes.*;
import com.olide.psi.*;
import com.intellij.navigation.ItemPresentation;

public class OlangPropertyImpl extends OlangNamedElementImpl implements OlangProperty {

  public OlangPropertyImpl(ASTNode node) {
    super(node);
  }

  public void accept(@NotNull OlangVisitor visitor) {
    visitor.visitProperty(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof OlangVisitor) accept((OlangVisitor)visitor);
    else super.accept(visitor);
  }

  public String getKey() {
    return OlangPsiImplUtil.getKey(this);
  }

  public String getValue() {
    return OlangPsiImplUtil.getValue(this);
  }

  public String getName() {
    return OlangPsiImplUtil.getName(this);
  }

  public PsiElement setName(String newName) {
    return OlangPsiImplUtil.setName(this, newName);
  }

  public PsiElement getNameIdentifier() {
    return OlangPsiImplUtil.getNameIdentifier(this);
  }

  public ItemPresentation getPresentation() {
    return OlangPsiImplUtil.getPresentation(this);
  }

  @Nullable
  @Override
  public PsiType getFunctionalInterfaceType() {
    return null;
  }

  @Override
  public boolean isAcceptable(PsiType psiType) {
    return false;
  }

  @Override
  public boolean isPotentiallyCompatible(PsiType psiType) {
    return false;
  }

  @Nullable
  @Override
  public PsiType getGroundTargetType(PsiType psiType) {
    return null;
  }

  @Nullable
  @Override
  public PsiType getType() {
    return null;
  }
}
