// This is a generated file. Not intended for manual editing.
package com.olide.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static com.olide.psi.OlangTypes.*;
import com.olide.psi.*;

public class OlangFnImpl extends OlangFunctionElementImpl implements OlangFn {

  public OlangFnImpl(ASTNode node) {
    super(node);
  }

  public void accept(@NotNull OlangVisitor visitor) {
    visitor.visitFn(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof OlangVisitor) accept((OlangVisitor)visitor);
    else super.accept(visitor);
  }

}
