// This is a generated file. Not intended for manual editing.
package com.olide.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface OlangFn extends OlangFunctionElement {

  //WARNING: getKey(...) is skipped
  //matching getKey(OlangFn, ...)
  //methods are not found in OlangPsiImplUtil

  //WARNING: getValue(...) is skipped
  //matching getValue(OlangFn, ...)
  //methods are not found in OlangPsiImplUtil

  //WARNING: getName(...) is skipped
  //matching getName(OlangFn, ...)
  //methods are not found in OlangPsiImplUtil

  //WARNING: setName(...) is skipped
  //matching setName(OlangFn, ...)
  //methods are not found in OlangPsiImplUtil

  //WARNING: getNameIdentifier(...) is skipped
  //matching getNameIdentifier(OlangFn, ...)
  //methods are not found in OlangPsiImplUtil

  //WARNING: getPresentation(...) is skipped
  //matching getPresentation(OlangFn, ...)
  //methods are not found in OlangPsiImplUtil

}
